
public class InputString {
	String valore;

	public InputString(String valore){
		this.valore=valore;
	}
	
	public String replace(){
		String formattedString = valore.replace('\n', ',');
		return formattedString;
	}
	
	public String decidiDelimitatore(){
		String  delimitatore=","; //default
		if(this.valore.startsWith("//")){
			delimitatore=""+this.valore.charAt(2);
		}
		System.out.println(delimitatore);
		return delimitatore;
		
	}
	
}
