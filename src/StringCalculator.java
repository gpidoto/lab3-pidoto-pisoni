import java.util.*;

public class StringCalculator {
	
	Integer stringaAddendi[] = null;
	
	public int add(String numbers) throws NumberFormatException{
		String delimiter =decidiDelimitatore(new InputString(numbers));
		numbers=tagliaStringa(numbers);
		numbers = this.formattaStringa(new InputString(numbers));
		if (numbers.contains(",,"))
			throw new NumberFormatException ("Input non corretto");
		int risultato = 0;
		StringTokenizer stringSplit = new StringTokenizer (numbers,delimiter);
		int parametri = stringSplit.countTokens();
		this.stringaAddendi = new Integer[parametri];
		int i = 0;
	
		while(stringSplit.hasMoreTokens()){
			String parametro = stringSplit.nextToken();
			stringaAddendi[i] = Integer.valueOf(parametro);
			i++;
			risultato+=Integer.valueOf(parametro);
		}
		return risultato; 
	}
	
	public Integer[] getLastSummands() {
	
		return this.stringaAddendi;
		
	}
	
	public String formattaStringa(InputString numbers){
		String formattedString = numbers.replace();
		return formattedString;
	}
	
	public String decidiDelimitatore(InputString numbers){
		String  delimitatore = numbers.decidiDelimitatore();
		return delimitatore;
		
	}
	
	public String tagliaStringa(String numbers){
		if (numbers.startsWith("//")) {
			int indice=numbers.indexOf('\n');
			numbers=numbers.substring(indice+1);
		}
		return numbers;
	}
	
}
