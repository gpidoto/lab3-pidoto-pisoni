import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class StringCalculatorTest {
	StringCalculator somma;
	@Before
	public void createSomma(){
		somma = new StringCalculator();
	}
	
	
	@Test
	public void test0() {
		int risultato = somma.add("");
		assertEquals(0, risultato);
	}
	
	@Test
	public void test1() {
		int risultato = somma.add("1");
		assertEquals(1, risultato);
	}
	
	@Test
	public void test2() {
		int risultato = somma.add("1,2");
		assertEquals(3, risultato);
	}
	
	@Test
	public void test3() {
		int risultato = somma.add("1,2,3,4,5");
		assertEquals(15, risultato);
	}

	@Test
	public void test4(){
		@SuppressWarnings("unused")
		int risultato = somma.add("1,2,3,4,5");
		Integer[] ultimi = somma.getLastSummands();
		Integer[] confronto = new Integer[5];
		for(int i=0; i<5; i++) {
			confronto[i]=i+1;
		}
		for (int i=0; i<5; i++) {
			assertEquals(confronto[i],ultimi[i]);
		}
	}
	
	@Test
	public void test5() {
		int risultato = somma.add("1\n2,3");
		assertEquals(6,risultato);
	}
	
	@Test(expected = NumberFormatException.class)
	public void test6(){
		@SuppressWarnings("unused")
		int risultato = somma.add("1,\n2");
	}
	
	@Test
	public void test7() {
		int risultato = somma.add("//;\n2;3");
		assertEquals(5,risultato);
	}
	
}
